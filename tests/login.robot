*** Settings ***
Documentation           Suite de teste de login do administrador

Resource                ../resources/base.robot

Suite Setup             Start Browser Session

Test Teardown           Take Screenshot 

#robot -d ../logs -v headless:false -v browser:firefox tests\
#robot -d ../logs -v headless:false -v browser:firefox tests/login.robot
# robot -d ../logs -i admin -v headless:false -v browser:firefox tests/login.robot 
*** Test Case ***
Login do Administrador
    [tags]          admin
    Go To Login Page
    Login With  admin@bodytest.com  pwd123
    User Should Be Logged In  Administrador

    [Teardown]      Clear LS And Take Screenshot

#  robot -d ../logs -i senhaIncorreta -v headless:false -v browser:firefox tests/login.robot 
Senha incorreta
    [tags]          senhaIncorreta
    Go To Login Page
    Login With                  admin@bodytest.com  abc123
    Toaster Text Should Be      Usuário ou senha inválido
    [Teardown]      Thinking And Take Screenshot        2

#robot -d ../logs -i emailIncorreto -v headless:false -v browser:firefox tests/login.robot
Email incorreto
    [tags]      emailIncorreto
    Go To Login Page
    Login With  admin&bodytest.com  abc123   
    Alert Text Should Be  Informe um e-mail válido   

#robot -d ../logs -i senhaNaoInformada -v headless:false -v browser:firefox tests/login.robot
Senha não informada
    [tags]      senhaNaoInformada
    Go To Login Page
    Login With  admin@bodytest.com  ${EMPTY}
    Alert Text Should Be  A senha é obrigatória  

#robot -d ../logs -i EmailNaoInformada -v headless:false -v browser:firefox tests/login.robot
Email não informado
    [tags]      EmailNaoInformada
    Go To Login Page
    Login With  ${EMPTY}  pwd123
    Alert Text Should Be  O e-mail é obrigatório 

#robot -d ../logs -i senhaEmailNaoInformada -v headless:false -v browser:chromium tests/login.robot
Email e Senha não informados
    [tags]      senhaEmailNaoInformada
    Go To Login Page

    Login With  ${EMPTY}  ${EMPTY}

    Alert Text Should Be   O e-mail é obrigatório
    Alert Text Should Be  A senha é obrigatória 
